rule connect_rules {
    strings: 
       
        $url = "github.com/csvl/SEMA-ToolChain" xor ascii
        //$url64 = "github.com/csvl/SEMA-ToolChain" base64("!@#$%^&*(){}[].,|ABCDEFGHIJ\x09LMNOPQRSTUVWXYZabcdefghijklmnopqrstu")
        //$urlwide = "github.com/csvl/SEMA-ToolChain" wide ascii
        $hex = {67 69 74 68 [5-50] 68 61 69 6e}
        $hexChain = {2f637376 [0-50] 6c2f5345 [0-50] 4d412d54 [0-50] 6f6f6c43 [0-50] 6861696e}
        $hexChainWide = {2f [0-3] 63 [0-3] 73 [0-3] 76 [0-3] 
                    6c [0-3] 2f [0-3] 53 [0-3] 45 [0-3] 
                    4d [0-3] 41 [0-3] 2d [0-3] 54 [0-3] 
                    6f [0-3] 6f [0-3] 6c [0-3] 43 [0-3] 
                    68 [0-3] 61 [0-3] 69 [0-3] 6e}
        

        $socket = /socket/
        $bind = /bind/
        $connect = /connect/

        $curl = /curl/
        $get = /GET/
        $mem = /memset/
        //$gc = /getc/


    condition:
        ( (any of ($url*) or any of ($hex*)) and ( $curl or ( ($connect or ($socket and $bind)) and ($get and $mem) ) ) )

}