rule read_rules {
    strings: 
        $a = "/home/user/key_rsa" wide ascii xor

        $hex = {2f 68 6f 6d [5-50] 72 73 61}
        //$hexwide = { 2f [0-3] 68 [0-3] 6f [0-3] 6d [0-3] 
        //             65 [0-3] 2f [0-3] 75 [0-3] 73 [0-3]
        //             65 [0-3] 72 [0-3] 2f [0-3] 6b [0-3]
        //             65 [0-3] 79 [0-3] 5f [0-3] 72 [0-3]
        //             73 [0-3] 61 }

        $hexbase = {4c 32 68 76 [5-50] 6e 4e 68}
        $reg = /\/hom.{,50}rs(a|f)/

        $r = /read/
        $s = /scan/
        $g = /gets/
        $gc = /getc/


    condition:
        (($a or $reg or (any of ($hex*))) and ($r or $s or $g or $gc))
}


rule write_rules {
    strings: 
        $b = /\/etc\/crontab/
        $c = "crontab" xor

        $w = /write/
        $e = /echo/

    condition:
        (($b or $c) and ($w or $e))
}