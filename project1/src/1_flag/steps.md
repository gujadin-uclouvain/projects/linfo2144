# Setps for Project 1

## Exercise 1 : The hidden flag

### With debugging symbols

- In Terminal
    - `gcc -g -o program program`
    - `gdb -q program`

- In GDB
    - `run main a a`
    - `disass check_login`
    - Find a address before the free of 'decoded_username' and 'decoded_password'
    - Set a breakpoint at this address
    - `start main a a`
    - Continue (with `c`) to the specified breakpoint
    - Analyse the stack with `i locals`
    - The values of 'decoded_username' and 'decoded_password' are the username and the password

### Without debugging symbols

- In Terminal
    - `gdb program`

- In GDB
    - `run main a a`
    - `disass decode_string`
    - Find a address of return of the function (= eax)
    - Set a breakpoint at this address
    - `start main a a`
    - Continue (with `c`) to the specified breakpoint
    - Analyse the register with `i r`
    - The values of eax is 'decoded_username'
    - Use `x/5c {address}` to translate the 5th hexa to char
    - Redo the op to find password
