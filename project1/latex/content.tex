\section{The hidden flag}
\emph{The address space randomization has been turned off for this question}\\

For this first problem, we needed to find the credential of a simple program to print the flag.

\subsection{Analysis of the source code}
To perform that, we first analysed the source code to find where the program should be exploited.\\

After looking closely at it, we found the decoded username and password were saved into two different variables into the \textit{check\_login} (which calls the \textit{decode\_string} function).\\
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{pictures/flag_src_functions.png}
    \caption{The functions \textit{check\_login} \& \textit{decode\_string}}
    \label{fig:flag-src}
\end{figure}
We will use this vulnerability to find the username, the password and so, the flag.\\

\subsection{Compilation of the source code \emph{with debugging symbols}}
To understand the pre-compiled program, we will compile the source code with \textbf{GCC} and the argument \textbf{-g} to enable the \emph{debugging symbols}.\\
It will help us to understand how the compiled program is organised.

\subsubsection{Analysis with GDB}
When we disassemble the \textit{check\_login} function, we saw as explained before the username and the password are saved into two variables and are free before the end of the \textit{check\_login} function.\\

We will then set a breakpoint between these two executions and analyse the stack with
\begin{lstlisting}[language=bash,caption={Command in GDB to analyse the stack}]
i locals
\end{lstlisting}
Thanks to that, we can clearly see the values of the \textit{decoded\_username} and \textit{decoded\_password} variables.

\subsection{Compilation of the source code \emph{without debugging symbols}}
Unfortunately for us, we were not allowed to recompile the source code with the debugging symbols, and the original one doesn't contain such feature.

\subsubsection{Analysis with GDB}
To find our flag, we will use then \textbf{GDB} to analyse the precompiled binary code (without debugging symbols) and we will disassemble the \textit{decode\_string} function. It is used by \textit{check\_login} to decode the correct encoded username and password.\\

We need to find the address of the return function command (to find \textit{eax} at the end) and set a breakpoint at this address. So, we could get the value returned by this function (which is firstly \textit{decoded\_username} and secondly \textit{decoded\_password}).\\

After that, we just need to start the \textit{main} function and move to our breakpoint previously set.\\

The main difference between the analysis with and without debugging symbols is we cannot analyse the stack and show us directly the variables saved in there.
So we must use another way and this is to analyse the register with the command:
\begin{lstlisting}[language=bash,caption={Command in GDB to analyse the \textit{eax} in the register}]
i r eax
\end{lstlisting}
Then we just need to take the address of \textit{eax} and use one of these commands:
\begin{lstlisting}[language=bash,caption={Command in GDB to translate the content of \textit{eax} into characters/string}]
x/6c {address_of_eax}
OR
x/s {address_of_eax}
\end{lstlisting}
It will print us the decoded username the \textit{decode\_string} function will return.\\

To obtain the decoded password, we just need to continue the process until reaching the same breakpoint and redo the same operation as before.\\

Finally, we have this kind of execution sequence:
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{pictures/flag_solution_without_debug_symb.png}
    \caption{Execution sequence in \textbf{GDB} to obtain the username and the password}
    \label{fig:flag-solution-without-debug-symb}
\end{figure}\\

In the end, if we take those values and put them into our program, we have found our flag.
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{pictures/flag_found.png}
    \caption{Found previous username and password to find the flag}
    \label{fig:flag-found}
\end{figure}

To run this solution, you can execute the GDB script named \textbf{program.gdb} which can be run with this command:
\begin{lstlisting}[language=bash,caption={Command in to launch our script in GDB}]
gdb --batch --command=program.gdb ./program
\end{lstlisting}
NB: This script needs the current user to have writing and reading access to the folder at the place he/she runs the command.

% ============================================================== %
\section{A restricted application}
During this exercise, we need to access the secret message without modifying the source code.

\subsection{Analysis of the source code}
At the first look at the source code (Figure~\ref{fig:secret-src}), we can notice the use of \textit{scanf} function will be useful to bypass all the checks established.\\

Indeed, this function has no limit of bits it can take in input. Therefore, we will produce a buffer overflow thanks to the \textit{scanf} function which initially modifies the \textit{username} variable to modify the \textit{age\_check2} variable without modifying the variables \textit{age} and \textit{age\_check1}.
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.6]{pictures/secret_src.png}
    \caption{The source code of the \textit{secret\_app} program}
    \label{fig:secret-src}
\end{figure}

\subsection{Compile with GCC}
In the beginning, we will recompile the source code with the debugging symbols (to make it easier and more readable).\\

Then, we launch the fleshly compiled program with \textbf{GDB} and our analysis can really start

\subsection{Analysis with GDB}
Firstly, we will disassemble the main function to analyse where it will be useful to start our research.

The first lines of the assembly code (Figure~\ref{fig:secret-disass-main}) explain to us the variables initiated at the beginning of the program are all stored in the register \textit{esp}.
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.6]{pictures/secret_disass_main.png}
    \caption{The disassembly code of the \textit{secret\_app} program}
    \label{fig:secret-disass-main}
\end{figure}

We will place two breakpoints at the most interesting place that is just before and after the \textit{memcpy} between variables \textit{secret} and \textit{secretMessage}.

We launch the function main in debug mode, we enter the username \textit{admin} and we move to our previously set breakpoints.\\

Then, we have to analyse where our variables are stored. For that, we will analyse the content of the \textit{esp} register thanks to this command:
\begin{lstlisting}[language=bash,caption={Command in GDB to display the content of esp}]
x/60wx $esp
\end{lstlisting}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{pictures/secret_result_esp_reg_before_memcpy.png}
    \caption{Result of the command \textit{x/60wx \$esp} before the memcpy}
    \label{fig:secret-result-esp-reg-before-memcpy}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7]{pictures/secret_result_esp_reg_after_memcpy.png}
    \caption{Result of the command \textit{x/60wx \$esp} after the memcpy}
    \label{fig:secret-result-esp-reg-after-memcpy}
\end{figure}

\newpage
As we can see in the figures~\ref{fig:secret-result-esp-reg-before-memcpy} and \ref{fig:secret-result-esp-reg-after-memcpy}, we can extract some useful informations:
\begin{itemize}
    \item At the address \textit{0xbffff2b4}, the value in \colorbox{red}{\textbf{red}} correspond to the \textit{username} variable.
    \item At the address \textit{0xbffff2f8}, the value in \colorbox{yellow}{\textbf{yellow}} correspond to the \textit{age} variable.
    \item At the address \textit{0xbffff2f8}, the value in \colorbox{orange}{\textbf{orange}} correspond to the \textit{age\_check1} variable.
    \item At the address \textit{0xbffff2f8}, the value in \colorbox{green}{\textbf{green}} correspond to the \textit{age\_check2} variable.
    \item The values between the \colorbox{red}{\textbf{red}} and \colorbox{green}{\textbf{green}} selections, correspond to the \textit{secret} variable.
\end{itemize}
As we can see, if we make a buffer overflow on the \textit{username} variable, we can modify the \textit{age\_check2}.\\
So, if we make a python script which generates the following input:
\begin{itemize}
    \item \textbf{The correct username}: \textit{admin}
    \item \textbf{The NULL character}: "\textit{\textbackslash x00}" ($\times 11$). It is used to fill the \textit{username} variable with only "\textit{admin}" in it.
    \item \textbf{A random character}: "\textit{A}" ($\times 48$). It is used to bypass the allocated memory for the \textit{secret} variable.
    \item \textbf{The value of the \textit{age\_check2}}: Change the two bytes (110) by \textit{"\textbackslash x00\textbackslash x01"} (1)
\end{itemize}
Finally, we can create this command to have access to the secret message:
\begin{lstlisting}[language=bash,caption={Solution}]
python2 -c "print 'admin' + '\x00'*11 + 'A'*48 + '\x01\x00'" | ./secret_app
\end{lstlisting}

\subsection{Fix for the vulnerability}
A possibility to fix this vulnerability is to only keep the 16 first char the user set in input.\\
To do that, we can change the line:
\begin{lstlisting}[language=bash,caption={Program with the vulnerability}]
scanf("%s", username);
\end{lstlisting}
with
\begin{lstlisting}[language=bash,caption={Program without the vulnerability}]
scanf("%16s", username);
\end{lstlisting}

% ============================================================== %
\section{An impossible race}
For the third problem, we needed to find a way to get the prize by winning the impossible race.

\subsection{Analysis of the source code}
First, we needed to see how to trigger the getYourPrize() function. To trigger it we need to change the car's title to "WIN". By looking at \textbf{struct car}we see that it takes 12 chars for its name and 4 chars for its title. By looking at \textbf{newCar} we see there is a malloc for the 16 chars but the title is never initialized.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.8]{pictures/racing1.png}
    \caption{The struct \textit{car} \& \textit{circuit}}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.8]{pictures/racing2.png}
    \caption{The function \textit{newCar}}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.7]{pictures/racing3.png}
    \caption{The functions \textit{newCircuit}}
\end{figure}

 By looking at the \textbf{newCircuit} we see that it uses a malloc of 16 for its name so we can use this input "WIN" in the car title. To do so we create a new circuit with a name where the first 12 chars do not matter and the next 3 chars are "WIN". Then we delete the circuit and create a new car. Now we have "WIN" at the address of the title of the car and we can claim our prize.

\subsection{Fix for the vulnerability}
We can fix this vulnerability by using the bytes at the address of \textit{title} in the car struct like this for example:
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{pictures/racing4.png}
    \caption{The functions \textit{newCircuit}}
\end{figure}

% ============================================================== %
\section{Find the flags}
For the final problem, we were given a compiled binary with no source code. The binary was compiled with a few vulnerabilities: no stack protection, no pie and the stack is executable.  So that means we can overflow buffers without alerting canaries, the code will always be loaded in the same virtual memory location and the stack being executable enables for easier shellcode injecting. 

\subsection{Disassembling the functions}
By using gdb and disassembling the functions we got the following pseudo-codes for each

\subsubsection{main}

\begin{algorithm}
\caption{Main function}\label{alg:cap}
\begin{algorithmic}
\State printing the secret\_function's address
\State vulnerable\_function()
\end{algorithmic}
\end{algorithm}

Here the only important thing the function does is use the vulnerable function for us to able to change the control flow of the program.

\subsubsection{vulnerable\_function}

\begin{algorithm}
\caption{vulnerable function}\label{alg:vuln}
\begin{algorithmic}
\State $buffer[24]$
\State $canary \gets 0xcafebabe $
\State print asking for input
\State $buffer \gets gets()$   \Comment{Vulnerable function which does not check the length of the input}
\If{$canary$ == $'0xcafebabe'$}
    \State prints the input
\Else{}
    \State puts() telling the canary has been modified
\end{algorithmic}
\end{algorithm}

Here we can see that there is a hardcoded canary in the code but it does nothing if we overwrite the values by overflowing the buffer. For the sake of the exercise, we will assume that if we do not bypass the canary the program just stops. We will be able to use the gets() function to overflow the buffer and overwrite the stored return pointer to execute or jump to any function in the code. 

\subsubsection{secret\_function}

\begin{algorithm}
\caption{secret function}\label{alg:cap}
\begin{algorithmic}
\State $buffer[256]$
\State $canary \gets 0xdeadbeef$
\If{($param\_1 == 0x12345678$) && ($param\_2 == 0x9abcdef0$)}
    \State flag1()  \Comment{outputs the flag}
    \State $buffer \gets gets()$
    \If {($canary == 0xdeadbeef$)}
        \State $key \gets key()$
        \State $val \gets strcmp(buffer, key)$
        \If {($val == 0$)}
            \State flag2() \Comment{outputs flag 2}
        \Else{}  
            \State puts(Key is incorrect)
    \Else{}   %a fix j'ai aucune idée de comment ca marche sur latex
        \State puts(Canary has been modified)
\Else{}
    \State puts(parameters are incorrect)
\end{algorithmic}
\end{algorithm}

Here we see that upon arriving at the function, 2 parameters are going to be checked so we must take this into account when making the payload. We then see another hardcoded canary check and the function asks us for the key to get the second flag.

\subsection{Flag 1}
Before starting we disable the Address Space Layout Randomization from the os with the following command so that we can benefit from the fact that the binary was compiled without PIE:

\begin{lstlisting}[language=bash,caption={Disabling ASLR}]
echo 0 | sudo tee /proc/sys/kernel/randomize\_va\_space 
\end{lstlisting}

For the first flag, we need to overflow the buffer of the \textbf{vulnerable function} while bypassing the canary. To do so we need 24 bytes to fill the buffer and then the 4 bytes of the \textbf{secret function} address. By using gdb we found that the canary checks for the value \textit{0xcafebabe} so we will input that in the payload at the spot where the canary is. By trial and error by looking at the values on the stack with \emph{GDB} we found out it is located 8 bytes into the buffer.
Now we can build our payload, for padding we will use uppercase A which is 41 in hex. The payload will look like this:
\begin{itemize}
    \item 8B padding + 8B canary + 24B padding + 4B secret fun address + 4B padding + 4B param 1 + 4B param 2 
\end{itemize}
The values and addresses need to be in the little-endian format and after inputting the payload, we get the following:

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{pictures/flag1issue.png}
    \caption{Flag 1}
\end{figure}

To run this solution execute \textbf{flag1.sh}\\

As you can see even though we got the first flag we encountered a pretty big problem, the null byte of the address makes the process think that we entered something for the key which is not the case. To work around it we will try to execute an 8-byte shellcode from the buffer in \textbf{vulnerable\_function()}.

\subsection{Key}
For this, we will try to find the address in gdb of the buffer in \textbf{vulnerable function} and find the offset by trial and error. For padding, we will use NOP this time and it will look like this:
\begin{itemize}
    \item 16B NOP + 8B Shellcode + 4B Buffer Address
\end{itemize}
Sadly we still got a segfault and after using the command \textit{dmesg} we see that it was generally a protection fault. So we have the wrong address or the operating system does not let us execute shellcode from the stack. 

% \subsection{Flag 2}





