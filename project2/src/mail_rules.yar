rule mail_rules {
    strings: 
       
        $email = "lingi2144@gmail.com"
        $emailSafe = "axel.legay+"
        $emailDom = /From:.{0,100}@uclouvain.be/
        $subject = /Subject:.{0,100}Solution/

        $isReply = "Reply-To"
        $reply = /Reply-to:.{0,100}lingi2144@gmail.com/
        $replyDom = /Reply-to:.{0,100}@uclouvain.be/



    condition:
      ( (not (any of ($email*))) or ($isReply and (not (any of ($reply*)))) )  and $subject
}